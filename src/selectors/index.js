import  {createSelector} from 'reselect'

const filtersGetter = state =>  state.filters;

const articlesGetter = state => state.articles;
export const  filtratedArticlesSelector = createSelector( articlesGetter ,filtersGetter, (articles, filters) => {
    const {selected} = filters;
   return articles.filter(article => {
        return (!selected.length || selected.includes(article.id));
    });
});

const idGetter = (state, props) => props.id;
const commentsGetter = state => state.comments;

export const commentSelectorFactory= () => createSelector (commentsGetter, idGetter, (comments, id)=>{
    return comments[id];
});
