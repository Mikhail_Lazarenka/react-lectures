export  const articles = [{
    "id": "1",
    "text": "111",
    "title": "t1",
    "comments": [
        {
            "id": "1",
            "user": "user1",
            "text": "c1fsdfdsfsdff"

        },
        {
            "id": 2,
            "user": "user2",
            "text": "c2fsdfdsfsdff"

        },
        {
            "id": "3",
            "user": "user3",
            "text": "c3fsdfdsfsdff"

        }
    ]
},
    {
        "id": 2,
        "text": "fsdfdsffs",
        "title": "t2",
        "comments": [ {
            "id": "4",
            "user": "user1",
            "text": "c4fsdfdsfsdff"

        },
            {
                "id": "5",
                "user": "user2",
                "text": "c5fsdfdsfsdff"

            },
            {
                "id": "6",
                "user": "user3",
                "text": "c6fsdfdsfsdff"

            }]
    },
    {
        "id": "3",
        "text": "a1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsd",
        "title": "t3",
        "comments": []


    },
    {
        "id": "4",
        "text": "a1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsd",
        "title": "t4",
        "comments": []
    },
];


export  const normalizedArticles = [{
    "id": "1",
    "text": "111",
    "title": "t1",
    "comments": ["1","2","3"]
},
    {
        "id": "2",
        "text": "a1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsd",
        "title": "t2",
        "comments": ["2","4"]
    },
    {
        "id": "3",
        "text": "a1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsd",
        "title": "t3",
        "comments": ["1"]


    },
    {
        "id": "4",
        "text": "a1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsda1sdfsfsdfafdfdasfasdfsdfsdfsdfdasffdsfdfsdfsdfsfsfsfsdfsdfsdfdsfdsfdsfdsfdfsdf\nsdfdsfdsfdsfdsfdsfdsfdsfs\nfsdfdsfsdfsfsfsd",
        "title": "t4",
        "comments": ["5","2"]
    },
];

export  const normalizedComments= [
    {
        "id": "1",
        "user": "user1",
        "text": "c1fsdfdsfsdff"

    },
    {
        "id": "2",
        "user": "user2",
        "text": "c2fsdfdsfsdff"
    },
    {
        "id": "3",
        "user": "user",
        "text": "c3fsdfdsfsdff"

    },
    {
        "id": "4",
        "user": "user4",
        "text": "c5fsdfdsfsdff"

    },
    {
        "id": "5",
        "user": "user5",
        "text": "c6fsdfdsfsdff"

    }
];