import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { CSSTransitionGroup } from 'react-transition-group'
import CommentList from "../CommentList";
import './article.css'
import article from "../../reducer/article";
import {connect} from 'react-redux';
import {deleteArticle} from "../../AC";


class Article extends Component{


    static propTypes = {
        article: PropTypes.shape({
            id: PropTypes.string.isRequired,
            text: PropTypes.string.isRequired,
            title: PropTypes.string.isRequired
        }).isRequired,
        isOpen: PropTypes.bool,
        toggleOpen: PropTypes.func
    };


    render(){
        const {article, isOpen, toggleOpen} = this.props;
        console.log('---', 'update article');

        return (
            <div ref={this.setContainerRef}>
                <div>{article.title}</div>
                <button onClick={toggleOpen}>
                    {isOpen ? 'close' : 'open'}
                </button>

                <CSSTransitionGroup
                    transitionName="article"
                    transitionEnterTimeout={500}
                    transitionLeaveTimeout={300}>

                    {this.getBody()}
                </CSSTransitionGroup>
            </div>
        )

    }

    setContainerRef = ref=>{
        this.container = ref;
    };

    handleDelete = ( )=>{
        const {deleteArticle, article} = this.props;
        deleteArticle(article.id);
        console.log('!!!!! DELETING ARTICLE !!!!');

    };

    getBody(){
        let {article, isOpen} = this.props;

        if(isOpen){
            return <section>
                {article.text}
                <CommentList comments={article.comments}/>
                <button onClick={this.handleDelete}>Delete Article</button>
                </section>;
        }else {
            return null;
        }
    }



}


export default  connect(null,{deleteArticle})(Article)
