import React from 'react'
import Article from "./Article";
import accordion from "../decorators/accordion";
import PropTypes from 'prop-types'
import  {connect} from 'react-redux'
import {filtratedArticlesSelector} from "../selectors";


class ArticleList extends React.Component{

    static propTypes = {
      articles: PropTypes.array,
        //from accordion
        openItemId: PropTypes.string,
        toggleOpenItem: PropTypes.func.isRequired
    };


    render(){

        const {articles, openItemId, toggleOpenItem} = this.props;

        const articleElement = articles.map(article=><li key={article.id}>
            <Article article={article}
                     isOpen={article.id === openItemId }
                     toggleOpen={toggleOpenItem(article.id)}
            />
        </li>);
        return (
            <div>
                {articleElement}
            </div>

        )

    }

}


export default connect((state) => {
    return{
        articles: filtratedArticlesSelector(state)
    };
    }
) (accordion(ArticleList));
