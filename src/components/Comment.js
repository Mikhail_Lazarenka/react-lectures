import React from  'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {commentSelector, commentSelectorFactory} from "../selectors";
function Comment({comment}) {
    return (
        <div>
            <p>{comment.text}<b>{comment.user}</b></p>
        </div>
    )
}
Comment.prototype = {
    id: PropTypes.string,
    //from connect
    comment: PropTypes.shape({
        text: PropTypes.string.isRequired,
        user: PropTypes.string.isRequired
    }).isRequired
};


export default connect((state, ownProps)=>{
    const commentSelector = commentSelectorFactory();
    return {
        comment: commentSelector(state, ownProps)
    }
})(Comment)