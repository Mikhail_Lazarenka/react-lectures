import React, {Component} from 'react';
import Select from 'react-select'
import PropTypes from 'prop-types'
import {changeSelection} from "../../AC";
import {connect} from 'react-redux'


class SelectFilter extends  Component{

    static propTypes = {
        articles: PropTypes.array.isRequired
    };

    handleChange = selected => {
      this.props.changeSelection(selected.map(option => option.value));
    };



    render(){
        const {articles, selected}= this.props;
        const options =articles.map(article =>({
            label: article.title,
            value: article.id
        }));

        return (
            <div>
                <Select options ={options} value ={selected}  multi ={true}  onChange = {this.handleChange}/>
            </div>
        )
    }

}

export default connect(state => ({
    selected: state.filters.selected,
    articles: state.articles
}), { changeSelection }) (SelectFilter)



