import React, {Component} from 'react';
import Select from './Select'
import DateRange from "./DateRange";
import PropTypes from 'prop-types'



function Filters(props) {
    return(
        <div>
            <DateRange/>
            <Select/>
        </div>
    )
}
Filters.propTypes = {
  articles: PropTypes.array.isRequired
};
export default Filters



