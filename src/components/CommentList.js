import React from 'react'
// import PropTypes from 'prop-types'
import toggleOpen from "../decorators/toggleOpen";
import Comment from "./Comment";
import CommentForm from "./CommentForm";


class CommentList extends React.Component{


    render(){

        const { isOpen, toggleOpen} = this.props;
        const text = isOpen ? 'show comments': 'hide comments';
        return (
            <div>
                <button onClick={toggleOpen}>{text}</button>
                {this.getBody()}
            </div>

        )

    }
    getBody = ()=>{
        const {comments, isOpen} = this.props;
        if(isOpen){
            return null;
        }
        if(!comments || !comments.length)
            return (
        <div>
            <p>No comments yet</p>
            <CommentForm/>
        </div>);

        return(
           <div>
               <ul>
                   {comments.map(id => <li key={id}><Comment id={id}/></li>)}
               </ul>
               <CommentForm/>
           </div>
        )
    }



}


export default toggleOpen(CommentList)
