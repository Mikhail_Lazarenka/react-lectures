import React, {Component} from 'react';
import ArticleList from "./ArticleList";
import UserForm from "./UserForm";
// import PropTypes from 'prop-types'
import 'react-select/dist/react-select.css'
import './CommentForm/style.css'
import Counter from "./Counter";
import Filters from "./Filters";
import {articles} from "../fixtures";
import 'react-select/dist/react-select.css'
// import 'article.css'

class App extends  Component{
    render() {
  
        return (
            <div>
                <Filters articles={articles}/>
                <Counter/>
                <UserForm/>
                <ArticleList />
            </div>
        );
    }
    
}


export default App;



