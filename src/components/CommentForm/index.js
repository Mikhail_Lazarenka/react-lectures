import  React from  'react'
import './style.css'

class CommentForm extends React.Component{
    state = {
        user: '',
        text: ''
    };

    render(){
      return (
          <div>
              <form onSubmit={this.handleSubmit}>
                  user: <input
                            value={this.state.user}
                            onChange = {this.handleChange('user')}
                            className = {this.getClassName('user')}
                        />
                  text: <input
                            value = {this.state.text}
                            onChange = {this.handleChange('text')}
                            className = {this.getClassName('text')}
                        />
                  <input type='submit' value = 'submit'/>
              </form>
          </div>
      )
    }

    handleSubmit =(ev)=>{
        ev.preventDefault();
        this.setState({
            user: '',
            text: ''
        })
    };

    handleChange = type => ev => {
        const {value} =ev.target;
        if(value.length>limits[type].max) return;
        this.setState({
            [type]: value
        })
    };

    getClassName = type => this.state[type].length && this.state[type].length < limits[type].min ? 'form-input_error' :'';
}

const   limits = {
    user: {
        min: 5,
        max: 10
    },
    text: {
        min: 5,
        max: 10
    }
};

export default CommentForm