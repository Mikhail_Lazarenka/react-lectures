import  {createStore, applyMiddleware} from 'redux'
import reducer from '../reducer'
import logger from '../middleware/logger'


const enhancer = applyMiddleware(logger);
const store  = createStore(reducer,{},enhancer);// принимает редьюсер т.е метод который будет управлять нашими даными

//dev only
window.store = store;

export default store
