import React, {Component as ReactComponent}from 'react'

export  default (Component)=> class Accordion extends ReactComponent{
    constructor(props){
        super(props);
        this.state = {
            openItemId: null
        };
    }

    render(){
        return <Component {...this.props} toggleOpenItem={this.toggleOpenItem} openItemId={this.state.openItemId}/>
    }



    toggleOpenItem = openItemId => (ev)=> {
        this.setState({
            openItemId: this.state.openItemId === openItemId ? null : openItemId
        })
    }
}